
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagner
 */
public class Pratica32 {
    private static double resultado;
    private static double verifica;
    private final static double x=-1, media=67, desvio=3;
    public static void main(String[] args) {
        resultado = densidade(x,media,desvio);
        //
           System.out.println("Densidade da Distribuição no ponto x = "+resultado+"\n");
           verifica=(1/(sqrt(2*Math.PI)*3));
           if (verifica==resultado) System.out.println("Resultado correto\n");
    }
    public static double densidade(double x, double media, 
            double desvio) {
        double d = (1/(sqrt(2*Math.PI)*desvio))*pow(Math.E,((-1/2)*pow(((x-media)/desvio),2)));
        d=Math.floor(d*10000)/10000;
        //d=d/10000;
        return d;
    }
}
